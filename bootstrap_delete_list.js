const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./testData.json');


    class Lists {
        
        static async getToken(url, args) {
            return fetch(`${url}/auth/login/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'  
                },
                body: JSON.stringify({
                    email: args.email,
                    password: args.password
                })
            }).then((res) => {
                return res.json();
            }).then((json) => {
                console.log(json)});
            
        };

        static async sendDeletingListRequest(url, access_token, listId) {
            return fetch(`${url}/user-lists/${listId}`, {
                method: 'DELETE',
                headers: {
                    'Authorization': `bearer ${access_token}`
                },
        });


    };


    static async getUserId(url, access_token){
        return fetch(`${url}/auth/login/`, {
            method: `GET`,
            headers: {
                'Authorization': `bearer ${access_token}` 
            },
    }).then((res) => {
        return res.json();
    })
};


    static async getListId(url, access_token, userId) {
        return fetch(`${url}/user-lists/${listId}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `bearer ${access_token}`
            },
    });
}
    }

    async function deleteList(url, args) {
        
            const authresponse = await Lists.getToken(url, args);
            const response = await Lists.sendDeletingListRequest(url, authresponse.data.access_token)
        

        
        if (response.status === 200) {
            console.log(`List is deleted`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully create list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    }

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Deleting list=========');
        
        deleteList(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            });
        
    });
}
