const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./testData.json');


    class Users {
        
        static async getToken(url, args) {
            return fetch(`${url}/auth/login/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'  
                },
                body: JSON.stringify({
                    email: args.email,
                    password: args.password
                })
            }).then((res) => {
                return res.json();
            })
            
        };


        static async getUserId(url, access_token){
            return fetch(`${url}/auth/me`, {
                method: `GET`,
                headers: {
                    'Authorization': `bearer ${access_token}` 
                },
        }).then((res) => {
            return res.json();
        })
    };


        static async sendDeletingUserRequest(url, access_token, userId) {
            return fetch(`${url}/users/${userId}`, {
                method: 'DELETE',
                headers: {
                    'Authorization': `bearer ${access_token}`
                }
        });


    };
}

    async function deleteUser(url, args) {
        
            const token = await Users.getToken(url, args);
            const userId = await Users.getUserId(url, token.data.access_token);
            const response = await Users.sendDeletingUserRequest(url, token.data.access_token, userId.data.id);
        

        
        if (response.status === 200) {
            console.log(`User is deleted`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully create list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    }

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Deleting user=========');
        
        deleteUser(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            });
        
    });
}
