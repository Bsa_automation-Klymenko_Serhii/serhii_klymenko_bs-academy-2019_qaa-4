const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./testData.json');


    class Lists {
        static async sendCreationListRequest(url, args, access_token) {
            return fetch(`${url}/user-lists`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${access_token}`
                },
                body: JSON.stringify({
                    name: args.listName
                })
            }).then((resList) => {
                return resList.json();
            });

        };
        
        

        static async getToken(url, args) {
            return fetch(`${url}/auth/login/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'  
                },
                body: JSON.stringify({
                    email: args.email,
                    password: args.password
                })
            }).then((res) => {
                return res.json();
            });
            
        };
    };

    async function listCreate(url, args) {
        
            const authresponse = await Lists.getToken(url, args);
            const response = await Lists.sendCreationListRequest(url, args, authresponse.data.access_token)
        

        
        if (response.status === 201) {
            console.log(`List is created`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully create list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    }

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Creating list=========');
        
        listCreate(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            });
        
    });
}
